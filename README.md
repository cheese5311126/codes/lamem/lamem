# LaMEM

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-bronze-orange)](https://eu.badgr.com/public/assertions/BKth1aOsRCqdLNhSKJjSZQ "SQAaaS bronze badge achieved")
[![GitLab Release (latest by SemVer)](https://img.shields.io/github/v/release/UniMainzGeo/LaMEM)](https://github.com/UniMainzGeo/LaMEM/releases)
[![DOI](https://zenodo.org/badge/641438500.svg)](https://zenodo.org/doi/10.5281/zenodo.10211634)

LaMEM is a parallel 3D numerical code that can be used to model various thermo-mechanical geodynamical processes
such as mantle-lithosphere interaction, or the dynamics of magmatic systems, for rocks that have visco-elasto-plastic or
(compressible) poroelastic rheologies. The code uses a marker-in-cell approach with a staggered finite difference
discretization and is built on top of PETSc. A range of (Galerkin) multigrid and iterative solvers are available, for both
linear and non-linear rheologies, using Picard and quasi-Newton solvers. LaMEM has been tested on a massively parallel
MPI-cluster with 458,752 cores and has checkpointing/restarting capabilities.

## Documentation

For further information, please visit the LaMEM 
[User Guide](https://unimainzgeo.github.io/LaMEM/dev).

## Developers

LaMEM is an open source code that was initially developed at the Johannes-Gutenberg University in Mainz (Germany). Many other colleagues have contributed to its development as well (see the documentation).
The key funding for the Mainz team came from:
- The European Research Council through Grants ERC StG 258830 (MODEL), ERC PoC 713397 (SALTED) and ERC CoG 771143 (MAGMA)
- The German ministry of Science and Eduction (BMBF) through projects SECURE, PERMEA, and PERMEA2.
- Priority programs of the German research foundation (DFG), specifically the [4DMB](http://www.spp-mountainbuilding.de) and [Habitable Earth](https://habitableearth.uni-koeln.de) projects. 

## License & Copyright

MIT License

Copyright (c) 2011-2023 JGU Mainz, Boris Kaus, Anton Popov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



